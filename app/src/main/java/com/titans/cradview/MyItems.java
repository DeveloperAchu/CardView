package com.titans.cradview;

import android.content.Context;

import java.util.ArrayList;

public class MyItems {
    private String name;

    public MyItems(String imageURL) {
        this.name = imageURL;
    }

    public String getName() {
        return name;
    }

    public static ArrayList<MyItems> getItemList(Context mContext) throws ArrayIndexOutOfBoundsException {
        String[] urls = mContext.getResources().getStringArray(R.array.names);

        ArrayList<MyItems> images = new ArrayList<>();

        for (int i = 0; i < urls.length; i++) {
            images.add(
                    new MyItems(urls[i])
            );
        }
        return images;
    }
}
