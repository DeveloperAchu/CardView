package com.titans.cradview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AdapterItems extends RecyclerView.Adapter<AdapterItems.ViewHolder> {

    private List<MyItems> ItemList;
    private Context context;
    private LayoutInflater inflater;

    public AdapterItems(ArrayList<MyItems> ItemList, Context context) {
        this.ItemList = ItemList;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;

        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.person_name);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View imageView = inflater.inflate(R.layout.card, parent, false);
        return new ViewHolder(imageView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MyItems items= ItemList.get(position);

        TextView textView = holder.name;
        textView.setText(items.getName());
    }

    @Override
    public int getItemCount() {
        return ItemList.size();
    }

}