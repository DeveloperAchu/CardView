package com.titans.cradview;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class MyListFragment extends Fragment {

    ArrayList<MyItems> items;
    RecyclerView recyclePersons;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.recycler_list, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        recyclePersons = (RecyclerView) getActivity().findViewById(R.id.recycler_list);
        items = MyItems.getItemList(getContext());

        AdapterItems adapter = new AdapterItems(items,getContext());
        recyclePersons.setAdapter(adapter);
        recyclePersons.setLayoutManager(new LinearLayoutManager(getContext()));
    }

}